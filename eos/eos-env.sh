#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'

# inspired from https://gitlab.cern.ch/ci-tools/ci-web-deployer
# but this one works with service accounts that even don't have ssh login privileges
# and has less surprises

export LOCAL_FOLDER=$1
export EOS_FOLDER=$2

if [[ -z "$AUTO_DEVOPS_CERNBOX_USER" ]]; then
  >&2 echo "missing EOS credentials env variable $$AUTO_DEVOPS_CERNBOX_USER"
  exit 1
fi
if [[ -z "$AUTO_DEVOPS_CERNBOX_PASS" ]]; then
  >&2 echo "missing EOS credentials env variable $$AUTO_DEVOPS_CERNBOX_PASS"
  exit 1
fi

! echo "$AUTO_DEVOPS_CERNBOX_PASS" | kinit "$AUTO_DEVOPS_CERNBOX_USER@CERN.CH" > /dev/null
if [[ ${PIPESTATUS[1]} -ne 0 ]]; then
  >&2 echo "kerberos auth failed for user $AUTO_DEVOPS_CERNBOX_USER"
  exit 1
fi
echo "kerberos auth succeeded"

export EOS_MGM_URL="root://eosuser.cern.ch"

case "$EOS_FOLDER" in
*/)
  ;;
*)
  export EOS_FOLDER="$EOS_FOLDER/"
  ;;
esac
cd $LOCAL_FOLDER
